NotifyBotStatusChange = {}
NotifyBotStatusChange.isBotRunning = false

function NotifyBotStatusChange.DoNotify()
		local script = [[
$ErrorActionPreference = "Stop"
$notificationTitle = "MinionApp stopped. This will disappear in 25s."

[Windows.UI.Notifications.ToastNotificationManager, Windows.UI.Notifications, ContentType = WindowsRuntime] > $null
$template = [Windows.UI.Notifications.ToastNotificationManager]::GetTemplateContent([Windows.UI.Notifications.ToastTemplateType]::ToastText01)
#Convert to .NET type for XML manipuration
$toastXml = [xml] $template.GetXml()
$toastXml.GetElementsByTagName("text").AppendChild($toastXml.CreateTextNode($notificationTitle)) > $null
$toastXml.SelectSingleNode("/toast").SetAttribute("duration","long")

#Convert back to WinRT type
$xml = New-Object Windows.Data.Xml.Dom.XmlDocument
$xml.LoadXml($toastXml.OuterXml)

$toast = [Windows.UI.Notifications.ToastNotification]::new($xml)
$toast.Tag = "PowerShell"
$toast.Group = "PowerShell"
$toast.ExpirationTime = [DateTimeOffset]::Now.AddSeconds(10)
#High pri
$toast.Priority(1)
#$toast.SuppressPopup = $true

$notifier = [Windows.UI.Notifications.ToastNotificationManager]::CreateToastNotifier("PowerShell")
$notifier.Show($toast);
]]
		local pipe = io.popen("powershell -command -", "w")
		pipe:write(script)
		--d(pipe:read("*all"))
		pipe:close()
end

function NotifyBotStatusChange.OnUpdate()
	if FFXIV_Common_BotRunning and not NotifyBotStatusChange.isBotRunning then
			NotifyBotStatusChange.isBotRunning = true
			--d("Bot started.")
			--NotifyBotStatusChange.DoNotify()
	elseif not FFXIV_Common_BotRunning and NotifyBotStatusChange.isBotRunning then
			NotifyBotStatusChange.isBotRunning = false 
			if gBotMode ~= GetString("assistMode") then --Ignore assist mode.
					d("Bot stopped.")
					NotifyBotStatusChange.DoNotify()
			end
	end
end

RegisterEventHandler("Gameloop.Update", NotifyBotStatusChange.OnUpdate, "NotifyBotStatusChanges")