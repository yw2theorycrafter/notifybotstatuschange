What it does:
 - Shows a desktop notification whenever Minion bot changes from running -> not running. This happens when you finish crafting, etc. Useful for hearing when the bot hits a snag and stops running and you're alt+tabbed or away from your computer.
 - It looks like this, and it plays the Windows notification sound effect (see below to customize the sound effect.)

 ![desktop notif](stopped_image.jpg)

 - It specifically doesn't for the "Assist" mode, because notifying on turning off assist would be obnoxious.

Installing:
 - Dependency: Install windows powershell. It might already be installed on your machine, otherwise look for it as an app or google it.
 - Copy NotifyBotStatusChange/ to make a new folder .../MINIONAPP/Bots/FFXIVMinion64/LuaMods/ffxivminion/NotifyBotStatusChange/
   - Tip: You can download this folder as a zip file, click "Download" at the top right of the screen.
 - Reload LUA
 
System configuration to work best:
 - This uses toast notifications that are new in Windows 10, I think.
 - For the notification to show while the game has focus and full screen, you need to allow notifications when playing a game, see 
 https://helpdeskgeek.com/windows-10/use-focus-assist-to-control-notifications-in-windows-10/
 - You probably want to set your "Notification" sound to be something loud, and turn up the system sound volume in your volume mixer. See:
 https://winaero.com/blog/change-or-disable-toast-notification-sound-in-windows-10/

